"""
This file describes the CNF class, which holds a CNF object.

"""
from LogicExpression import Expression, isAtomic, isNested
from LogicExpression import isNegation, isConjunction, \
    isDisjunction, isImplication, isEquivalence

class CNF(object):
    def __init__(self, expression):
        #Expression class handles the validation of the input
        self.expression = Expression(expression)
        self.exps = []                              #expressions holder, for folding purposes etc
        self.cnf = []
        self.clauses = []
        self.toCnf()

    def toCnf(self):
        #procedure:
        #   1) simplify equivalence and implies
        #   2) move negations inward
        #   3) apply distributive
        self.exps = [self.expression.expression]

        if isAtomic(self.expression):
            self.cnf = [[self.expression.expression]]
        else:
            while True:
                for index, exp in enumerate(self.exps):
                    #handles equivalences
                    if isEquivalence(exp):
                        cs = foldEquiv(exp)
                        self.exps.pop(index)
                        for c in cs:
                            self.exps.append(c)
                        continue #restart
                    #handles implications
                    if isImplication(exp):
                        cs = foldImply(exp)
                        self.exps.pop(index)
                        for c in cs:
                            self.exps.append(c)
                        continue
                    #handle negations
                    if isNegation(exp):
                        cs = foldNegations(exp)
                        if exp != cs:
                            self.exps.pop(index)
                            self.exps.append(cs)
                            continue
                    #handle distributive
                    cs = foldDist(exp)
                    if cs != exp:
                        self.exps.pop(index)
                        self.exps.append(cs)
                        continue
                    #if reach here, everything is clean -> break
                break
            for exp in self.exps:
                if isDisjunction(exp):
                    disj = [i for i in exp[1:]]
                    self.clauses.append(disj)
                if isConjunction(exp):
                    for conj in exp[1:]:
                        self.cnf.append([conj])
            for c in self.clauses:
                self.cnf.append(c)
        #self.cnf = [[c] for c in self.clauses]

def foldEquiv(exp):
    if isinstance(exp, Expression):
        exp = exp.expression
    clause_1 = ("=>", exp[1], exp[2])
    clause_2 = ("=>", exp[2], exp[1])
    return [clause_1, clause_2]

def foldImply(exp):
    if isinstance(exp, Expression):
        exp = exp.expression
    clause_1 = ("not", exp[1])
    clause_2 = exp[2]
    return [clause_1, clause_2]

def foldNegations(sentence):
    #passes on non negation, or negation of atomic sentence
    if not isNegation(sentence):
        return sentence
    if isinstance(sentence[1], str):
        return sentence

    #handles negatiof of negations
    negations_count = 0
    if isNegation(sentence[1]):
        while True:
            if isNegation(sentence[1]):
                negations_count += 1
                sentence = sentence[1]
            else:
                break
        if negations_count % 2 == 1:
            return sentence[1]
        elif negations_count % 2 == 0:
            return sentence

    #handles negation of clause
    if isDisjunction(sentence[1]):
        items = [item for item in sentence[1][1:]]
        return_obj = ("and", )
        for item in items:
            return_obj += (("not", item), )
        return return_obj

    #hanles negation of conjunctions
    if isConjunction(sentence[1]):
        items = [item for item in sentence[1][1:]]
        return_obj = ("or", )
        for item in items:
            return_obj += (("not", item), )
        return return_obj

def foldDist(expression):
    #rejects un nested expressions
    if not isNested(expression):
        return expression

    exp_1, exp_2 = expression[1], expression[2]
    #handle double nested
    if (not isAtomic(exp_1) and not isAtomic(exp_2)):
        return foldDouble(expression)
    #handle simple nested
    if (isAtomic(exp_1) and not isAtomic(exp_2)):
        return foldSimple(expression, exp_1, exp_2)
    if (isAtomic(exp_2) and not isAtomic(exp_1)):
        exp_1, exp_2 = exp_2, exp_1
        return foldSimple(expression, exp_1, exp_2)

#foldDist auxiliary functions
#exp_2 must be the nested one!
def foldSimple(expression, exp_1, exp_2):
    op_1, op_2 = expression[0], exp_2[0]
    folded = (op_2, )
    for item in exp_2[1:]:
        folded += ((op_1, exp_1, item), )
    return folded

def foldDouble(expression):
    exp_1, exp_2 = expression[1], expression[2]
    op_inner, op_main = expression[0], exp_1[0]
    items_1, items_2 = exp_1[1:], exp_2[1:]

    folded = (op_main, )
    for i in items_1:
        partial = (op_main, )
        for j in items_2:
            partial += ((op_inner, i, j), )
        folded += ((partial), )
    return folded
