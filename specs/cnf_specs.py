from CnfConverter import foldEquiv, foldImply, foldNegations, foldDist
from LogicExpression import Expression
from CnfConverter import CNF
import unittest

class testCnf(unittest.TestCase):
    def testAtomicCNF(self):
        cnf_obj = CNF("Socrates")
        cnf_exp = cnf_obj.cnf
        expected = [["Socrates"]] #note: conjunction of clauses, where the unique clause is "Socrates"
        self.assertEqual(expected, cnf_exp)
        cnf_obj = CNF(("not","Socrates"))
        cnf_exp = cnf_obj.cnf
        expected = [[("not", "Socrates")]] #note: conjunction of clauses, where the unique clause is "Socrates"
        self.assertEqual(expected, cnf_exp)


    def testSimpleDisjunction(self):
        exp = ("or", "A", "B", "C")
        cnf_obj = CNF(exp)
        expected = [["A", "B", "C"]]
        self.assertEqual(expected, cnf_obj.cnf)

    def testSimpleConjunction(self):
        exp = ("and", "A", "B", "C")
        cnf_obj = CNF(exp)
        expected = [["A"], ["B"], ["C"]]
        self.assertEqual(expected, cnf_obj.cnf)

    def testSimpleImply(self):
        exp = ("=>", "A", "B")
        expected = [[("not", "A"), "B"]]
        self.assertEqual(expected, CNF(exp).cnf)

    def testSimpleEquiv(self):
        exp = ("<=>", "A", "B")
        expected = [ [("not", "A"), "B"], [("not", "B"), "A"] ]
        self.assertEqual(expected, CNF(exp).cnf)



class testCnfMethods(unittest.TestCase):
    def testFoldEquiv(self):
        exp = ("<=>", "A", "B")
        exptected = [("=>", "A", "B"), ("=>", "B", "A")]
        self.assertEqual(exptected, foldEquiv(exp))

        expr = ("<=>", "A", "B")
        exp = Expression(expr)
        exptected = [("=>", "A", "B"), ("=>", "B", "A")]
        self.assertEqual(exptected, foldEquiv(exp))

        exp = ("<=>", ("or", "A", "B"), "C")
        exptected = [("=>", ("or", "A", "B"), "C"), ("=>", "C", ("or", "A", "B"))]
        self.assertEqual(exptected, foldEquiv(exp))

    def testFoldImply(self):
        exp = ("=>", "A", "B")
        expected = [("not", "A"), "B"]
        self.assertEqual(expected, foldImply(exp))

        expr = ("=>", "A", "B")
        exp = Expression(expr)
        expected = [("not", "A"), "B"]
        self.assertEqual(expected, foldImply(exp))

        exp = ("=>", "A", ("or", "B", "C"))
        exptected = [("not", "A"), ("or", "B", "C")]
        self.assertEqual(exptected, foldImply(exp))

    def testSimpleFoldNegations(self):
        exp = ("not", "A")
        self.assertEqual(exp, foldNegations(exp))
        exp = ("not", ("not", "A"))
        self.assertEqual("A", foldNegations(exp))
        exp = ("not", ("not", ("not", "A")))
        self.assertEqual(("not", "A"), foldNegations(exp))
        exp = ("not", ("not", ("not", ("not", "A"))))
        self.assertEqual("A", foldNegations(exp))

    def testDisjFoldNegations(self):
        exp = ("not", ("or", "A", "B"))
        exptected = ("and", ("not", "A"), ("not", "B"))
        self.assertEqual(exptected, foldNegations(exp))

        exp = ("not", ("or", "A", "B", "C"))
        exptected = ("and", ("not", "A"), ("not", "B"), ("not", "C"))
        self.assertEqual(exptected, foldNegations(exp))

    def testConjFoldNegations(self):
        exp = ("not", ("and", "A", "B"))
        expected = ("or", ("not", "A"), ("not", "B"))
        self.assertEqual(expected, foldNegations(exp))

    def testFoldDist_1(self):
        exp = ("or", "A", ("and", "B", "C"))
        expected = ("and", ("or", "A", "B"), ("or", "A", "C"))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_2(self):
        exp = ("or", "P", ("and", "Q", "R"))
        expected = ("and", ("or", "P", "Q"), ("or", "P", "R"))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_3(self):
        exp = ("and", "A", ("and", "B", "C"))
        expected = ("and", ("and", "A", "B"), ("and", "A", "C"))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_4(self):
        exp = ("=>", "P", ("<=>", "Q", "R"))
        expected = ("<=>", ("=>", "P", "Q"), ("=>", "P", "R"))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_5(self):
        exp = ("=>", "P", ("=>", "Q", "R"))
        expected = ("=>", ("=>", "P", "Q"), ("=>", "P", "R"))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_6(self):
        exp = ("or", ("and", "P", "Q"), ("and", "R", "S"))
        expected = ("and", ("and", ("or", "P", "R"), ("or", "P", "S")), ("and", ("or", "Q", "R"), ("or", "Q", "S")))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_7(self):
        exp = ("and", ("or", "P", "Q"), ("or", "R", "S"))
        expected = ("or", ("or", ("and", "P", "R"), ("and", "P", "S")), ("or", ("and", "Q", "R"), ("and", "Q", "S")))
        self.assertEqual(expected, foldDist(exp))

    def testFoldDist_8(self):
        exp = ("and", "A", "B")
        self.assertEqual(exp, foldDist(exp))

if __name__ == "__main__":
    unittest.main()














#lalal
