from LogicExpression import Expression
from LogicExpression import isAtomic, isNested, isEqual
from LogicExpression import isNegation, isConjunction, \
    isDisjunction, isImplication, isEquivalence
import unittest


class testExpressionValidation(unittest.TestCase):
    def testRejectNumber(self):
        self.assertRaises(TypeError, Expression, 42)

    def testRejectList(self):
        self.assertRaises(TypeError, Expression, ["not", "A"])

    def testRejectBadNot(self):
        self.assertRaises(TypeError, Expression, ("not", "A", "B"))

    def testRejectBadNestedNot(self):
        self.assertRaises(TypeError, Expression, ("not", ("or", "A")))

    def testRejectBadImplies(self):
        self.assertRaises(TypeError, Expression, ("=>", "A"))
        self.assertRaises(TypeError, Expression, ("<=>", "A"))
        self.assertRaises(TypeError, Expression, ("=>", "A", "B", "c"))
        self.assertRaises(TypeError, Expression, ("<=>", "A", "B", "C"))

    def testRejectBadNestesImplies(self):
        self.assertRaises(TypeError, Expression, ("=>", "A", ("not", "B", "C")))
        self.assertRaises(TypeError, Expression, ("<=>", "A", ("not", "B", "C")))

    def testRejectBadAO(self):
        self.assertRaises(TypeError, Expression, ("and", "A"))
        self.assertRaises(TypeError, Expression, ("or", "A"))

    def testRejectBadNestedAO(self):
        self.assertRaises(TypeError, Expression, ("or", "A", ("not", "B", "C")))
        self.assertRaises(TypeError, Expression, ("and", "A", ("not", "B", "C")))

    def testAcceptStr(self):
        a = Expression("A")
        self.assertEqual(a.expression, "A")

    def testAcceptSimpleNot(self):
        exp = ("not", "A")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)

    def testAcceptNestedNot(self):
        exp = ("not", ("or", "A", "B"))
        a = Expression(exp)
        self.assertEqual(a.expression, exp)

    def testAcceptSimpleImply(self):
        exp = ("=>", "A", "B")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("<=>", "A", "B")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)

    def testAcceptNestedImply(self):
        exp = ("=>", ("=>", "A", "B"), "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("<=>", ("<=>", "A", "B"), "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)

    def testAcceptSimpleAO(self):
        exp = ("and", "A", "B")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("or", "A", "B")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("and", "A", "B", "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("or", "A", "B", "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)

    def testAcceptNestedAO(self):
        exp = ("and", ("and", "A", "B"), "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)
        exp = ("or", ("and", "A", "B", "D"), "C")
        a = Expression(exp)
        self.assertEqual(a.expression, exp)


class testExpressionType(unittest.TestCase):
    def testIsAtomic(self):
        exp = "Socrates"
        a = Expression(exp)
        self.assertEqual(True, isAtomic(a.expression))
        exp = ("not", "Socrates")
        a = Expression(exp)
        self.assertEqual(True, isAtomic(a.expression))

    def testIsNotAtomic(self):
        exp = ("not", ("and", "A", "B"))
        a = Expression(exp)
        self.assertEqual(False, isAtomic(a.expression))

    def testIsNegation(self):
        exp = ("not", ("and", "A", "B"))
        a = Expression(exp)
        self.assertEqual(True, isNegation(a))

    def testIsConjunction(self):
        exp = ("and", "A", "B")
        a = Expression(exp)
        self.assertEqual(True, isConjunction(a))

    def testIsDisjunction(self):
        exp = ("or", "A", "B")
        a = Expression(exp)
        self.assertEqual(True, isDisjunction(a))

    def testIsImplication(self):
        exp = ("=>", "A", "B")
        a = Expression(exp)
        self.assertEqual(True, isImplication(a))

    def testIsEquivalence(self):
        exp = ("<=>", "A", "B")
        a = Expression(exp)
        self.assertEqual(True, isEquivalence(a))


class testExpressionMethods(unittest.TestCase):
    def testIsNotNested(self):
        exp = ("not", "Socrates")
        self.assertEqual(False, isNested(exp))
        exp = "Socrates"
        self.assertEqual(False, isNested(exp))

    def testIsNested(self):
        exp = ("not", ("or", "A", "B"))
        self.assertEqual(True, isNested(exp))
        exp = ("and", "A", "B")
        self.assertEqual(False, isNested(exp))

    def testIsEqual(self):
        exp_1 = ("or", "A", "B")
        exp_2 = ("or", "B", "A")
        self.assertEqual(True, isEqual(exp_1, exp_2))
        exp_1 = ("and", "A", "B", "C")
        exp_2 = ("and", "B", "C", "A")
        self.assertEqual(True, isEqual(exp_1, exp_2))
        exp_1 = ("<=>", "A", "B")
        exp_2 = ("<=>", "B", "A")
        self.assertEqual(True, isEqual(exp_1, exp_2))

    def testIsNotEqual(self):
        exp_1 = ("or", "A", "B")
        exp_2 = ("and", "B", "A")
        self.assertEqual(False, isEqual(exp_1, exp_2))
        exp_1 = ("=>", "A", "B")
        exp_2 = ("=>", "B", "A")
        self.assertEqual(False, isEqual(exp_1, exp_2))

if __name__ == "__main__":
    unittest.main()














#lalala


















#lalalala
