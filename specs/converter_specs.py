import unittest
import convert
from convert import foldEquiv

class testSentencesType(unittest.TestCase):
    def testIsAtomic(self):
        sentence_1 = "Socrates is a man"
        sentence_2 = ("not", sentence_1)
        sentence_3 = ("or", sentence_1, sentence_2)
        sentence_4 = ("not", ("or", "A", "B"))
        self.assertEqual(True, convert.isAtomic(sentence_1))
        self.assertEqual(True, convert.isAtomic(sentence_2))
        self.assertEqual(False, convert.isAtomic(sentence_3))
        self.assertEqual(False, convert.isAtomic(sentence_4))

    def testIsNegation(self):
        sentence_valid = ("not", ("or", "A", "B"))
        sentence_invalid = ("or", "A", "B")
        self.assertEqual(True, convert.isNegation(sentence_valid))
        self.assertEqual(False, convert.isNegation(sentence_invalid))

    def testIsDisjunction(self):
        sentence_valid = ("or", "A", "B")
        sentence_invalid = ("not", ("or", "A", "B"))
        self.assertEqual(True, convert.isDisjunction(sentence_valid))
        self.assertEqual(False, convert.isDisjunction(sentence_invalid))

    def testIsConjunction(self):
        sentence_valid = ("and", "A", "B")
        sentence_invalid = ("not", ("and", "A", "B"))
        self.assertEqual(True, convert.isConjunction(sentence_valid))
        self.assertEqual(False, convert.isConjunction(sentence_invalid))

    def testIsImplication(self):
        sentence_valid = ("=>", "A", "B")
        sentence_invalid = ("<=>", ("and", "A", "B"))
        self.assertEqual(True, convert.isImplication(sentence_valid))
        self.assertEqual(False, convert.isImplication(sentence_invalid))

    def testIsEquivalence(self):
        sentence_valid = ("<=>", "A", "B")
        sentence_invalid = ("=>", ("and", "A", "B"))
        self.assertEqual(True, convert.isEquivalence(sentence_valid))
        self.assertEqual(False, convert.isEquivalence(sentence_invalid))


class testCnfInput(unittest.TestCase):
    def testInvalidType(self):
        sentence = ['A']
        self.assertRaises(TypeError, convert.toCnf, sentence)

    def testInvalidOperator(self):
        sentence = ("xor", "A", "B")
        self.assertRaises(TypeError, convert.toCnf, sentence)


    def testStringInput(self):
        sentence = "Socrates"
        expected_result = (["Socrates"], )
        self.assertEqual(expected_result, convert.toCnf(sentence))
    """
    def testDisjunctionInput(self):
        sentence = ("or", "A", "B")
        expected_result = (["A", "B"], )
        self.assertEqual(expected_result, convert.toCnf(sentence))

        sentence = ("or", ("or", "A", "B"), "C")
        expected_result = (["A", "B", "C"], )
        self.assertEqual(expected_result, convert.toCnf(sentence))
    """

class testCnfFunctions(unittest.TestCase):
    def testFoldEquiv(self):
        sentence = ("<=>", "A", "B")
        expected_result = [("=>", "A", "B"), ("=>", "B", "A")]
        self.assertEqual(expected_result, convert.foldEquiv(sentence))

    def testFoldImply(self):
        sentence = ("=>", "A", "B")
        expected_result = [("not", "A"), "B"]
        self.assertEqual(expected_result, convert.foldImply(sentence))

    def testFoldNegations(self):
        sentence = ("not", "A")
        self.assertEqual(sentence, convert.foldNegations(sentence))

        sentence = ("or", "A", "B")
        self.assertEqual(sentence, convert.foldNegations(sentence))

        sentence = ("not", ("not", "A"))
        self.assertEqual("A", convert.foldNegations(sentence))

        sentence = ("not", ("or", "A", "B"))
        expected_result = ("and", ("not", "A"), ("not", "B"))
        self.assertEqual(expected_result, convert.foldNegations(sentence))

        sentence = ("not", ("and", "A", "B"))
        expected_result = ("or", ("not", "A"), ("not", "B"))
        self.assertEqual(expected_result, convert.foldNegations(sentence))

    def testFoldDist(self):
        sentence = ("not", "A")
        self.assertEqual(sentence, convert.foldDist(sentence))

        sentence = ("or", "A", "B")
        self.assertEqual(sentence, convert.foldDist(sentence))

        sentence = ("or", "A", ("and", "B", "C"))
        expected_result = ("and", ("or", "A", "B"), ("or", "A", "C"))
        self.assertEqual(expected_result, sentence)

        sentence = ("or", ("and", "A", "B"), ("and", "C", "D"))
        expected_result = ("and", ("or", "A", "B"), ("or", "A", "C"))
        self.assertEqual(expected_result, sentence)


if __name__ == "__main__":
    unittest.main()
