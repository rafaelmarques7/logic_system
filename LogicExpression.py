"""
An expression is a well formed logical sentence.
It can either be a string, representing a symbol,
or it can be a tuple, where the first index represents the operation,
and the following index represent symbols or other expressions .

The Expression class has some properties which allow an easy
expression verification, manipulation, and formating.
"""

class Expression(object):
    def __init__(self, expression):
        self.expression = None
        self.validateInput(expression)

    def validateInput(self, expression):
        if isinstance(expression, str):
            self.expression = expression
        elif isinstance(expression, tuple):
            if len(expression) < 2:
                raise TypeError("expression tuple length has to be >= 2")
            else:
                self.validateInputTuple(expression)
        else:
            raise TypeError("expression must be string or tuple")

    def validateInputTuple(self, expression):
        #discards bad operators
        self.validateOperator(expression)
        #discards bad formatten not - requires length 2 tuple
        self.validateNegations(expression)
        #accepts ors and ands of any length >= 3
        self.validateAO(expression)
        #discards bad formattion implies and equivalence - requires length 3 tuple
        self.validateImplies(expression)

    def validateOperator(self, expression, save = True):
        operators = operators = ["and", "or", "not", "=>", "<=>"] #valid operators
        operator = expression[0]
        if operator not in operators:
            raise TypeError("invalid expression operator: " + str(operator) + "\tmust\
                    be one of the following\n" + str(operators))
        else:
            if save:
                self.operator = operator

    def validateNegations(self, expression):
        if self.operator == "not":
            if len(expression) != 2:
                raise TypeError("negation must be tuple with length 2, ('not', 'expression')")
            else:
                #validates inner expression
                negated_expression = Expression(expression[1])
                self.expression = expression

    def validateAO(self, expression):
        if self.operator == "or" or self.operator == "and":
            if len(expression) < 3:
                raise TypeError("conjunctions and disjunctions must have tuple length >= 3")
            else:
                #validates inner expressions
                valid_expressions = [Expression(e) for e in expression[1:]]
                self.expression = expression

    def validateImplies(self, expression):
        if self.operator == "=>" or self.operator == "<=>":
            if len(expression) != 3:
                raise TypeError("implication and equivalence must be \
                        tuple with length 3, ('=>/<=>', 'expression_1', 'expression_2')")
            else:
                #validates inner expressions
                valid_expressions = [Expression(e) for e in expression[1:]]
                self.expression = expression


def objectToExpression(exp):
    if isinstance(exp, Expression):
        expression = exp.expression
    else:
        expression = exp
    return expression

def isAtomic(exp):
    expression = objectToExpression(exp)
    if type(expression) is str:
        return True
    if type(expression) is tuple:
        operator = expression[0]
        if len(expression) != 2:
            return False
        if operator != "not":
            return False
        if isAtomic(expression[1]):
            return True
        else:
            return False

def isNegation(exp):
    expression = objectToExpression(exp)
    e, o = expression, expression[0]
    if (not type(e) is tuple) or (len(e) != 2) or (o != "not"):
        return False
    return True

def isConjunction(exp):
    expression = objectToExpression(exp)
    e, o = expression, expression[0]
    if (not type(e) is tuple) or (len(e) < 3) or (o != "and"):
        return False
    return True

def isDisjunction(exp):
    expression = objectToExpression(exp)
    e, o = expression, expression[0]
    if (not type(e) is tuple) or (len(e) < 3) or (o != "or"):
        return False
    return True

def isImplication(exp):
    expression = objectToExpression(exp)
    e, o = expression, expression[0]
    if (not type(e) is tuple) or (len(e) != 3) or (o != "=>"):
        return False
    return True

def isEquivalence(exp):
    expression = objectToExpression(exp)
    e, o = expression, expression[0]
    if (not type(e) is tuple) or (len(e) != 3) or (o != "<=>"):
        return False
    return True

def isNested(expression):
    if isAtomic(expression):
        return False
    #validates expression
    valid_expressions = [Expression(e) for e in expression[1:]]
    for e in valid_expressions:
        if not isAtomic(e):
            return True
    return False

def isEqual(exp_1, exp_2):
    #validate expressions
    valid_expressions_1 = [Expression(e) for e in exp_1[1:]]
    valid_expressions_2 = [Expression(e) for e in exp_2[1:]]
    #validate operators
    op_1, op_2 = exp_1[0], exp_2[0]
    if op_1 != op_2:
        return False
    if op_1 == "and" or op_1 == "or" or op_1 == "<=>":
        set_1 = set((e for e in exp_1[1:]))
        set_2 = set((e for e in exp_2[1:]))
        if set_1 == set_2:
            return True
        return False
    if op_1 == "=>":
        a, b, c, d = exp_1[1], exp_1[2], exp_2[1], exp_2[2]
        if a == c and b == d:
            return True
        return False












#lalala
