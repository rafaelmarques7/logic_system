"""
    This file should convert any sentence to its Clausal Norm Form - CNF -

"""



#this functions is only called if sentence is an equivalence
def foldEquiv(sentence):
    clause_1 = ("=>", sentence[1], sentence[2])
    clause_2 = ("=>", sentence[2], sentence[1])
    return [clause_1, clause_2]

def foldImply(sentence):
    clause_1 = ("not", sentence[1])
    clause_2 = sentence[2]
    return [clause_1, clause_2]

def foldNegations(sentence):
    #passes on non negation, or negation of atomic sentence
    if not isNegation(sentence):
        return sentence
    if isinstance(sentence[1], str):
        return sentence

    #handles negatiof of negations
    if isNegation(sentence[1]):
        return sentence[1][1]
    #handles negation of clause
    if isDisjunction(sentence[1]):
        a, b = sentence[1][1], sentence[1][2]
        #this should be interpreted as a conjunction: nA AND nB
        return ("and", ("not", a), ("not", b))
    #hanles negation of conjunctions
    if isConjunction(sentence[1]):
        a, b = sentence[1][1], sentence[1][2]
        #this should be interpreted as a disjunction: nA OR nB
        return ("or", ("not", a), ("not", b))

def foldDist(sentence):
    if not isConjunction(sentence):
        return sentence

class CNF(object):
    #input can either be a sentence, from stdin for example
    #or a file. in this instance, the object has to be initialised first, and
    #the transformations occurs after file specification
    def __init__(self, sentence = None):
        self.cnf = []               #list of conjunctions
        self.clauses = []           #list of clauses
        if sentence != None:
            self.validateInput(sentence)

        #iterative process to transform sentence into cnf type object
        #start iteration with received sentence
        self.transform(sentence)

    #cleans input
    def validateInput(self, sentence):
        #validate type
        if not isinstance(sentence, (tuple, str)):
            raise TypeError("sentence must be type 'tuple' or 'str'")
        #validate tuple body
        if isinstance(sentence, tuple):
            if not len(sentence) in [2,3]:
                raise TypeError("wrong tuple size, must be 2 or 3")
            #validate tuple operator
            self.operators = ["and", "or", "not", "=>", "<=>"]
            if sentence[0] not in self.operators:
                raise ValueError("sentence operator: '{0}'is not valid", (sentence[0]))
        #accept validated sentence
        self.sentence = sentence

    def transform(self, sentence):
        #handles atomic sentence
        if isAtomic(sentence):
            self.clauses.append(sentence)

        #handles deconstruction type
        if isEquivalence(sentence):
            self.handleEquivalence(sentence)
        if isImplication(sentence):
            self.handleImplication(sentence)

        #cleans negations
        self.handleNegation(sentence)
        #apply distributive
        self.handleDistributive(sentence)

        #finish every transformation with cnf update
        self.cnf = [clause for clause in self.clauses]



    def checkDist(self, sentence):
        #passes on non negation, or negation of atomic sentence
        if isAtomic(sentence):
            return sentence
        if not isDisjunction(sentence):
            return sentence

        a, b = sentence[1], sentence[2]
        if isAtomic(a) and isConjunction(b):
            c, d = b[1], b[2]
            return [("or", a, c), ("or", a, d)]
        if isConjunction(a) and isAtomic(b):
            c, d = a[1], a[2]
            return [("or", c, b), ("or", d, b)]
        """
        if isConjunction(a) and isConjunction(b):
            a, b, c, d = a[1], a[2], b[1], b[2]
            return
        """
    def isCnf(self, cnf_obj):
        for conjunction in cnf_obj:
            for clause in conjunction:
                if not isAtomic(clause):
                    return False
        return True




#receives sentence as input,
#returns sequence of disjunctions, where each is a list of literal
def toCnf(sentence):
    #raises error if input is invalid
    validateInput(sentence)

    #handles atomic sentence
    if isAtomic(sentence):
        disjunction = [sentence]
        return (disjunction, )

    #handles complex sentences
    operator = sentence[0] #due to validateInput(), we know the operator is valid
    #handles conjunction
    if operator == "or":
        sent1, sent2 = sentence[1], sentence[2]
        if isAtomic(sent1) and isAtomic(sent2):
            disjunction = [sent1, sent2]
            return (disjunction, )


#return True on "P" (str) and on "not P" (tuple (not, "P"))
def isAtomic(sentence):
    if type(sentence) is str:
        return True
    if type(sentence) is tuple:
        if len(sentence) != 2:
            return False
        operator = sentence[0]
        if operator != "not":
            return False
        if isAtomic(sentence[1]):
            return True
        else:
            return False

def isNegation(sentence):
    if (not type(sentence) is tuple) or (len(sentence) != 2) or (sentence[0] != "not"):
        return False
    return True

def isConjunction(sentence):
    if (not type(sentence) is tuple) or (len(sentence) != 3) or (sentence[0] != "and"):
        return False
    return True

def isDisjunction(sentence):
    if (not type(sentence) is tuple) or (len(sentence) != 3) or (sentence[0] != "or"):
        return False
    return True

def isImplication(sentence):
    if (not type(sentence) is tuple) or (len(sentence) != 3) or (sentence[0] != "=>"):
        return False
    return True

def isEquivalence(sentence):
    if (not type(sentence) is tuple) or (len(sentence) != 3) or (sentence[0] != "<=>"):
        return False
    return True

def validateInput(sentence):
    if not (type(sentence) is str or type(sentence) is tuple):
        raise TypeError("sentence has to be a string (atomic sentence) or a tuple")
    if type(sentence) is tuple:
        operators = ["and", "or", "not", "=>", "<=>"] #valid operators
        operator = sentence[0]
        if not (operator in operators):
            raise TypeError("sentence operator is not valid")
